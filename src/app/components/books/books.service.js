(function() {
  'use strict';

  angular
      .module('materialSaWdRu')
      .factory('books', books);

  /** @ngInject */
  function books($resource) {

    return {
        itself: $resource('https://ds.aggregion.com/api/public/catalog/:book', {book:'@book'}),
        bundles: $resource('https://ds.aggregion.com/api/public/catalog/:book/bundles', {book:'@book'})
    };

  }

})();
