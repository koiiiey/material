
(function() {
  'use strict';

  angular
      .module('materialSaWdRu')
      .directive('errSrc', error);

  /** @ngInject */
  function error() {
    return {
      link: linkFunc
    };

    function linkFunc(scope, element, attrs) {

      element.bind('error', function() {
        if (attrs.src != attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });

    }

  }

})();
