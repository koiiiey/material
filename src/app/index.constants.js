/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('materialSaWdRu')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
