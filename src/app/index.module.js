(function() {
  'use strict';

  angular
    .module('materialSaWdRu', ['ngResource', 'ui.router', 'ngMaterial', 'toastr']);

})();
