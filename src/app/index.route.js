(function() {
  'use strict';

  angular
    .module('materialSaWdRu')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('list', {
          url: '/',
          templateUrl: 'app/list/list.html',
          controller: 'ListController',
          controllerAs: 'list'
        });

    $stateProvider
        .state('404', {
          url: '/404',
          templateUrl: 'app/main/main.html',
          controller: 'MainController',
          controllerAs: 'main'
        });

    $stateProvider
        .state('one', {
          url: '/one/:id',
          templateUrl: 'app/one/one.html',
          controller: 'OneController',
          controllerAs: 'one'
        });

    $urlRouterProvider.otherwise('/404');
  }

})();
