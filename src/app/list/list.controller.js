(function() {
  'use strict';

  angular
    .module('materialSaWdRu')
    .controller('ListController', ListController);



  /** @ngInject */
  function ListController(books, $log, $state) {
    var vm = this;

    vm.classAnimation = '';
    vm.creationDate = 1463078800841;
    vm.tiles = [];
    vm.state = $state;

    books.itself.query().$promise.then(buildGridModel).catch(errorShow);


    function errorShow(){

    }

    function buildGridModel(list){

      $log.log(list);

      var tileTmpl = {
        background: ""
      };

      var it, results = [ ];
      for (var j=0; j<list.length; j++) {
        it = angular.extend(list[j],tileTmpl);
        it.icon  = it.icon + (j+1);
        it.span  = { row : 1, col : 1 };
        switch(j+1) {
          case 1:
            it.background = "red";
            it.span.row = it.span.col = 2;
            break;
          case 2: it.background = "green";         break;
          case 3: it.background = "darkBlue";      break;
          case 4:
            it.background = "blue";
            it.span.col = 2;
            break;
          case 5:
            it.background = "yellow";
            it.span.row = it.span.col = 2;
            break;
          case 6: it.background = "pink";          break;
          case 7: it.background = "darkBlue";      break;
          case 8: it.background = "purple";        break;
          case 9: it.background = "deepBlue";      break;
          case 10: it.background = "lightPurple";  break;
          case 11: it.background = "yellow";       break;
        }

        if(!it.previewImages || !it.previewImages.length){
          it.previewImages = [];
          it.previewImages[0] = '12ce171be47031a58f6d12ddefca93d52bda709b1b720d50cf48747d6cd44cb6';
        }

        results.push(it);
      }
      vm.tiles = results;
      //return results;
    }
  }
})();
