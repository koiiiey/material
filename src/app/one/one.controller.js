(function() {
  'use strict';

  angular
    .module('materialSaWdRu')
    .controller('OneController', OneController);



  /** @ngInject */
  function OneController($stateParams, books, $log, $state) {
    var vm = this;

    $log.log('logged:',$stateParams);

    vm.classAnimation = '';
    vm.creationDate = 1463078800841;
    vm.tiles = [];
    vm.booksData = [
      {face: '---', who: ''}
    ];

    books.itself.get({book: $stateParams.id}).$promise.then(buildModel).catch(errorShow);
    books.bundles.query({book: $stateParams.id}).$promise.then(buildBundles).catch(errorShow);


    function errorShow(){
      $state.go('404');
    }

    function buildBundles(list){
      $log.log('bundles', list);
      vm.booksData.bundles = list;
    }

    function buildModel(bookData){

      $log.log('book', bookData);
      vm.booksData = bookData;


    }
  }
})();
